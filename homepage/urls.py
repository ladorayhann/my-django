from django.urls import path
from . import views



urlpatterns = [
    path('', views.index, name='index'),
    path('resume/', views.resume, name='resume'),
    path('about/', views.about, name='about'),
    path('galeri/', views.galeri, name='galeri'),
    path('contact/', views.contact, name='contact'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/clear', views.schedule_clear, name='schedule_clear'),
    path('schedule/delete/<int:id>', views.schedule_delete, name='schedule_delete'),





]
